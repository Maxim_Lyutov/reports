#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);
use IO::File;
use File::Basename;
use MIME::Base64;
use Log::Log4perl qw(:easy);
use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;

our $EXCHANGE_DIR = $VAR_DIR.'/exchange';
our $PID_FILE = $VAR_DIR.'/run/'.basename($0).'.pid';
my ($script_file_name) = basename($0) =~ m/^(.*?)\.[^.]+$/;

#&zabbix_send(1);

my $log_layout = '[%d{yyyy-MM-dd HH:mm:ss}] %p %m';
Log::Log4perl->easy_init(
	{
		level	=> $INFO,
		file => ">>$LOG_DIR/$script_file_name.log",
		utf8 => 1,
		layout => $log_layout
	},
	{
		level => $INFO,
		file => 'STDOUT',
		utf8 => 1,
		layout => $log_layout
	}
);
my $logger = Log::Log4perl->get_logger();

&echo("Начало работы PID $$\n");
our $pid = &create_pid_file($PID_FILE);
exit unless($pid == $$);

&echo("Установка соединения с базой данных '$DB->{'db'}' на '$DB->{'host'}'\n");
our $dbh = &db_connect($DB);

#-------------------------------------------------------------------------------------------------
sub echo($) {
	my ($str) = @_;
	$logger->info($str);
}

#-------------------------------------------------------------------------------------------------
sub create_pid_file($) {
	my ($pid_file_name) = @_;
	if (-e $pid_file_name) {
		&echo("Найден PID файл\n");
		my $fh = IO::File->new($pid_file_name) or die "Не могу прочитать PID файл $pid_file_name: $!\n";
		my $pid = <$fh>;
		chomp($pid);
		if(defined($pid) && kill(0, $pid)) {
			&echo("Найден работающий процесс с PID $pid\n");
			return $pid;
		}
		unless(-w $pid_file_name && unlink $pid_file_name) {
			die "Не могу удалить PID файл $pid_file_name\n";
		}
	}
	my $fh = IO::File->new($pid_file_name, O_WRONLY|O_CREAT|O_EXCL, 0644) or die "Не могу создать PID файл $pid_file_name: $!\n";
	print $fh $$;
	close($fh);
	return $$;
}

#-------------------------------------------------------------------------------------------------
sub query_to_file($$$) {
	my ($dbh, $query, $file_name) = @_;
	my $aref = $dbh->selectall_arrayref($query) or die "Не могу выполнить запрос: $DBI::errstr\n";
	open($TABLE, "> $file_name") or die "Не могу создать файл $file_name: $!\n";
	foreach my $row (@{$aref}) {
		my @parced_row = ();
		foreach my $cell (@{$row}) {
		  my $field = &parse_field($cell, 'other');
			$field =~ s/\\/\\\\/g;
			push(@parced_row, $field);
		}
		print $TABLE join("\t", @parced_row)."\n";
	}
	close($TABLE);
	return scalar(@{$aref});
}

#-------------------------------------------------------------------------------------------------
sub query_to_excel($$$) {
	my ($dbh, $query, $file_name) = @_;
	my $sth = $dbh->prepare($query) or die 'Ошибка подготовки запроса! '.$DBI::errstr;
	$sth->execute() or die 'Ошибка выполнения запроса! '.$DBI::errstr;
	my $filed_names_aref = $sth->{'NAME'};
	my $result_aref = $sth->fetchall_arrayref() or die 'Ошибка получения данных! '.$DBI::errstr;
	$sth->finish;
	
	my $workbook  = Excel::Writer::XLSX->new($file_name);
	$workbook->set_tempdir($TMP_DIR);
	my $worksheet = $workbook->add_worksheet();
	$worksheet->keep_leading_zeros();
	
	my $row = 0;
	$worksheet->write_row($row++, 0, $filed_names_aref);
	$worksheet->write_col($row, 0, $result_aref);
	$workbook->close;
	undef $workbook;
}

#-------------------------------------------------------------------------------------------------
sub Base64_UTF8($) {
	my ($str) = @_;
	utf8::encode($str);
	my $s = MIME::Base64::encode($str, '');
	return '=?utf-8?B?'.$s.'?=';
}

#-------------------------------------------------------------------------------------------------
sub is_email_list($) {
	local $_ = shift;
	return m|${qr/^$EMAIL_REGEXP(\s*[,;]\s*$EMAIL_REGEXP)*$/}|;
}

#-------------------------------------------------------------------------------------------------
sub zabbix_send($) {
	my $param = shift;
	my $zabbix_sender = '/bin/zabbix_sender';
	if (-x $zabbix_sender) {
		&Run("echo \"- $script_file_name $param\" | $zabbix_sender -z zabbix.ak.local -s `hostname -f` -i -");
	}
}

#-------------------------------------------------------------------------------------------------
BEGIN {
	$SIG{__DIE__} = sub {
		return if $^S;
		my $msg = shift;
		unless (utf8::is_utf8($msg)) {
			utf8::decode($msg);
		}
		$logger->fatal('АВАРИЙНОЕ ЗАВЕРШЕНИЕ РАБОТЫ!!! '.$msg);
		exit;
	};
	$SIG{__WARN__} = sub{
		my $msg = shift;
		unless (utf8::is_utf8($msg)) {
			utf8::decode($msg);
		}
		$logger->warn($msg);
	};
	$SIG{INT} = $SIG{TERM} = sub {
		$logger->fatal("ПРИНУДИТЕЛЬНОЕ ЗАВЕРШЕНИЕ РАБОТЫ!\n");
		exit;
	};
}

#-------------------------------------------------------------------------------------------------
END {
	if (defined($dbh)) {
		&echo("Отключение от базы данных '$DB->{'db'}' на '$DB->{'host'}'\n");
		$dbh->disconnect();
	}
	unlink $PID_FILE if ($pid == $$);
	&echo("Окончание работы PID $$\n\n");
#	&zabbix_send(0);
}

1;
