#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);
use FindBin qw($Bin);
use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;
use File::Path;
require $Bin.'/../lib/common.pl';
require $Bin.'/../lib/bin.pl';

my $REPORTS_DIR = $VAR_DIR.'/reports';

%hierarchy_auto = (
	truck => 'Грузовые',
	#car => 'Легковые'
);

foreach my $key (keys %hierarchy_auto){
	&echo("Формируем отчет по коэффициентам для '$hierarchy_auto{$key}'\n");
	
	my $sth = $dbh->prepare("select * from portal.build_report_k('$hierarchy_auto{$key}');") or die 'Ошибка подготовки запроса! '.$DBI::errstr;
	
	$sth->execute() or die 'Ошибка выполнения ! '.$DBI::errstr;
	my $report_aref = $sth->fetchall_arrayref({}) or die 'Ошибка получения данных! '.$DBI::errstr;
	my @filed_name = @{$sth->{'NAME'}};
	$sth->finish ;
	
	my $workbook  = Excel::Writer::XLSX->new("$REPORTS_DIR/${key}_prices.xlsx");
	$workbook->set_tempdir($TMP_DIR);
	my $worksheet = $workbook->add_worksheet("Отчет $hierarchy_auto{$key}");
	
	my $row = 0;
	my $col = 0;
	
	foreach my $field (@filed_name){
		$worksheet->write_string($row, $col++, $field);
	}
	
	my $row = 1;
	foreach my $href_row (@{$report_aref}) {
		my $col = 0;
		foreach my $field (@filed_name) {
			$worksheet->write_string($row, $col++, defined($href_row->{$field}) ? $href_row->{$field} : '');
		}
		$row++;
	}

	$workbook->close;
	undef $workbook;
}
