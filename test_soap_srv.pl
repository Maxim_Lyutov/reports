#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);
use FindBin qw($Bin);
use Mail::Sender;
use Cache::Memcached;

require $Bin.'/../lib/common.pl';
require $Bin.'/../lib/bin.pl';
require $Bin.'/../lib/soap.pl';

$Mail::Sender::NO_X_MAILER = 1;

my $cache_key = 'isError';
my $cache = Cache::Memcached->new(
        servers => ['127.0.0.1:11200'],
        namespace => 'd-soap-proxy::'
    );

my $timeCache = 3600;
my $Subject;
my $Message;

my $answer = &soap_request('ping', '');

if (0+ $answer->{'result'}) {
    if ($cache->get($cache_key) eq '1'){
       $cache->set($cache_key, '0', $timeCache);
       $Subject = 'ВНИМАНИЕ: Отправка заказов работает';
       $Message = 'ВНИМАНИЕ: Веб-сервис по отправке заказов в MS DAX работает!';
    }
}
else {
    &echo("ОШИБКА: $answer->{'errorMessage'}\n");
    $Subject = 'ОШИБКА: Отправка заказов не работает';
    $Message = "ОШИБКА: Веб-сервис по отправке заказов в MS DAX не работает!\n $answer->{'errorMessage'}";
    $cache->set($cache_key, '1', $timeCache );
}

if (defined($Subject)){

    my $sender = new Mail::Sender({smtp => $SMTP_SERVER, from => &Base64_UTF8('Сайт autont.ru').' <mail@autont.ru>', on_errors => undef});
    $sender->OpenMultipart({
       to => 'n.kkov@autont.ru, s.co@autont.ru , m.lov@autont.ru',
       
       subject => &Base64_UTF8($Subject)
    });
    $sender->Body({charset => 'UTF8'});
    $sender->SendLineEnc($Message);

    warn "Не удалось отправить почту: $Mail::Sender::Error\n" unless ref $sender;
    $sender->Close() or warn $sender->{'error_msg'}."\n";
}
