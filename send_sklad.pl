#!/usr/bin/perl

use utf8; 
use open qw(:std :utf8);
use FindBin qw($Bin);
use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;
use File::Path;
use Mail::Sender;
use Encode;
no warnings 'layer';

$Mail::Sender::NO_X_MAILER = 1;
require $Bin.'/../lib/common.pl';
require $Bin.'/../lib/bin.pl';

my $REPORTS_DIR = $VAR_DIR.'/reports';
my $file_report = '';
my $input_data = [
	{   
		name_suppliers => 'Берг',
		email => 'price@auto.ru',
		warehouse => '101'
	},
	{	
		name_suppliers => 'Автоспутник',
		email => 'price@auto.ru',
		warehouse => '801'
	},
	{
		name_suppliers => 'Юником',
		email => 'price@auto.ru',
		warehouse => '901'
	},
	{
		name_suppliers => 'Партком',
		email => 'price@auto.ru',
		warehouse => '901'
	},
	{
		name_suppliers => 'Иксора',
		email => 'price@auto.ru',
		warehouse => '802'
	},
	{
		name_suppliers => 'Росско',
		email => 'price@auto.ru',
		warehouse => '701'
	},
		
];

my %worksheet_font = (
	font => 'Calibri-Light',
	size => 12
);

&echo("Формируем отчет по КД по выборочным прайс-листам...\n");

foreach my $arow (@{$input_data}){
	$file_report = "$REPORTS_DIR/$arow->{'warehouse'}.xlsx" ;
	
	my $book  = Excel::Writer::XLSX->new($file_report);
	my $sheet = $book->add_worksheet("Отчет для $arow->{'name_suppliers'}");
	my $def_format = $book->add_format(%worksheet_font);
	
	my $fields = [
		{
			name => 'brand_name',
			title => 'Бренд',
			format => $def_format
		},
		{
			name => 'code',
			title => 'Артикул',
			format => $def_format
		},
		{	
			name => 'package',
			title => 'Кол-во',
			format => $def_format
		},
		{
			name => 'price',
			title => 'Цена',
			format => $def_format
		},
	
	];

	my $href_res = $dbh->selectall_arrayref("select * from public.get_parts_stock_odds($arow->{'warehouse'})", {Slice => {}}) or die 'Ошибка получения данных! '.$DBI::errstr;
	my $col = $row = 0 ;

	foreach my $head (@{$fields}){
		$sheet->write( $row, $col++ , $head->{'title'}, $def_format);
	}
	$row = 1;
	foreach my $hVal (@{$href_res}){
		$col = 0;
		foreach my $Value (@{$fields}){
			$sheet->write( $row, $col++ , $hVal->{$Value->{'name'}}, $def_format);
		}
		$row++;
	}

	$book->close();
	my $sender = Mail::Sender->new({
		smtp => $SMTP_SERVER,
		from => &Base64_UTF8('Прайс-лист для поставщика ').$arow->{'name_suppliers'}.' <mail@autosrv.ru>',
		on_errors => undef
	});

	my $klient_emails = $ak_emails = $arow->{'email'};

	my(undef,undef,undef,$day,$mon,$year,undef,undef,undef) = localtime(time);
	my $current_date = $day.($mon += 1).($year += 1900) ;

	&echo("\n$file_report -> $klient_emails\n");
	
	$sender->OpenMultipart({
			to => $klient_emails,
			bcc => $ak_emails,
			subject => &Base64_UTF8('АвтоСервис '.$current_date)
		});
	$sender->Body({charset => 'UTF8'});
	$sender->Attach({
			file => $file_report
		});
	warn "Не удалось отправить почту: $Mail::Sender::Error\n" unless ref $sender;  
	$sender->Close() or warn $sender->{'error_msg'}."\n";

	unlink $file_report;
}
&echo("Окончание работы скрипта...\n");
