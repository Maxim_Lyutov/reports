#!/bin/bash

SOURCE_DIR=images  # источник с картинками
WORK_DIR=ttt      # временный каталог для обработки файлов
RESULT_DIR=out     # каталог для результатов 
SITE_DIR=media     # каталог для сайта (условно)

Echo(){
 echo $(date +"[%Y-%m-%d %H:%M:%S]") $1
}

[ -d $RESULT_DIR ] || rm -r $RESULT_DIR > /dev/null 2>&1 
mkdir $RESULT_DIR

rm -r $WORK_DIR > /dev/null 2>&1
mkdir $WORK_DIR

export PGPASSWORD=111122
PG_CONN='psql -h localhost -p 5402 -U portal_user -w -d dev_site -t -A -c '

for BRAND_ID in $(ls $SOURCE_DIR | grep -E ^[0-9]{3}$)
do

	BRAND_NAME=$($PG_CONN "select web.get_brand_name('$BRAND_ID')")
	if (( $? != 0 )); then
		Echo "Ошибка вызова фунции web.get_brand_name()";
		exit 1
	fi
	
	if [ $BRAND_NAME ]; then
		
		Echo "Нашли бренд: $BRAND_ID $BRAND_NAME :"
		
		[ -d $WORK_DIR/$BRAND_ID ] || mkdir $WORK_DIR/$BRAND_ID > /dev/null 2>&1  
		[ -d $RESULT_DIR/$BRAND_ID ] || mkdir $RESULT_DIR/$BRAND_ID > /dev/null 2>&1
		
		NOMER_FILE_DOUBLE=1
		
		for FULL_PATH_IMAGE in $(find $SOURCE_DIR/$BRAND_ID -type f)
		do	
			GET_XY_IMG=( $(identify -format "%[fx:w] %[fx:h]" $FULL_PATH_IMAGE) )
			
			if (( $? == 0 )); then
				FILE_NAME=${FULL_PATH_IMAGE##*/}
				FILE_SHORT_NAME=${FILE_NAME%.*}
				FILE_SHORT_NAME="$(cut -d'_' -f1 <<< $FILE_SHORT_NAME)"
				FILE_EXT_NAME="$(cut -d'_' -f2 <<< $FILE_SHORT_NAME)"
				
				PART_ID=$($PG_CONN "select web.get_part_id('$BRAND_NAME', '$FILE_SHORT_NAME' )")
				if (( $? != 0 )); then
					Echo "Ошибка вызова фунции web.get_part_id()";
					exit 2
				fi
				
				if [ $PART_ID ]; then
					Echo "Найден код товара : $PART_ID "
					
					cp $FULL_PATH_IMAGE $WORK_DIR/$BRAND_ID/$FILE_NAME > /dev/null 2>&1
					
					if (( ${GET_XY_IMG[0]} > ${GET_XY_IMG[1]} )); then
						PARAM_RESIZE='800'
					else
						PARAM_RESIZE='x800'
					fi
					
					LEVEL_QUALITY=('-quality 20%', '-quality 40%', '-quality 60%', '')
					
					FILES=(${FILE_SHORT_NAME}_20.jpg , ${FILE_SHORT_NAME}_40.jpg , ${FILE_SHORT_NAME}_60.jpg , ${FILE_SHORT_NAME}_png.png )
					
					SIZE=0
					INDEX_QUAL=0
					convert $WORK_DIR/$BRAND_ID/$FILE_NAME -resize $PARAM_RESIZE ${LEVEL_QUALITY[$INDEX_QUAL]} -strip $WORK_DIR/$BRAND_ID/${FILES[$INDEX_QUAL]}
					SIZE_MIN=$(stat -c %s $WORK_DIR/$BRAND_ID/${FILES[$INDEX_QUAL]})
					
					RESULT_FILE=${FILES[$INDEX_QUAL]}
					
					for((INDEX_QUAL=1; INDEX_QUAL < 4; INDEX_QUAL++))
					do
						convert $WORK_DIR/$BRAND_ID/$FILE_NAME -resize $PARAM_RESIZE ${LEVEL_QUALITY[$INDEX_QUAL]} -strip $WORK_DIR/$BRAND_ID/${FILES[$INDEX_QUAL]}
						SIZE=$(stat -c %s $WORK_DIR/$BRAND_ID/${FILES[$INDEX_QUAL]})
						
						if (( $SIZE<=$SIZE_MIN )); then
							SIZE_MIN=$SIZE
							RESULT_FILE=${FILES[$INDEX_QUAL]}
						fi
					done
					
					PATH_FROM_FILE=$WORK_DIR/$BRAND_ID/$RESULT_FILE
					PATH_TO_FILE=$RESULT_DIR/$BRAND_ID/$PART_ID.${RESULT_FILE##*.}
					
					if [ -e $PATH_TO_FILE ]; then
						NOMER_FILE_DOUBLE=${NOMER_FILE_DOUBLE+1}
						PATH_TO_FILE=$RESULT_DIR/$BRAND_ID/${PART_ID}_$NOMER_FILE_DOUBLE.${RESULT_FILE##*.}
						#Echo $PATH_TO_FILE
					fi
					
					cp $PATH_FROM_FILE $PATH_TO_FILE > /dev/null 2>&1
					Echo "Получил файл : $PATH_TO_FILE"
					
					unset LEVEL_QUALITY
					unset FILES
					
				fi # $part_id
			fi
			
		done
	fi
done

mv $SITE_DIR $(date +"%Y_%m_%d_%H%M%S")
mv $RESULT_DIR $SITE_DIR

Echo "Обработка завершена."
exit 0
